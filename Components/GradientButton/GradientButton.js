//Author : Rohan Patel
import React from "react";
import Styles from "./GradientButtonStyle";
import {LinearGradient} from "expo-linear-gradient";
import {Text, TouchableOpacity, View} from "react-native";

const GradientButton = props => {
    return (
        <View style={Styles.buttonContainer}>
            <LinearGradient
                colors={["#FE2382", "#fa8748"]}
                style={Styles.gradientContainer}
                start={[0.0, 0.5]}
                end={[1.0, 0.5]}
                locations={[0.0, 1.0]}
            >
                <TouchableOpacity
                    style={Styles.touchable}
                    onPress={props.onPressHandler}
                >
                    <Text style={Styles.text}>{props.text}</Text>
                </TouchableOpacity>
            </LinearGradient>
        </View>
    );
};

export default GradientButton;