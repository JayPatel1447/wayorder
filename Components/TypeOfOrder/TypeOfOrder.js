//Author : Rohan Patel
import React from "react";
import {View,Text,TouchableOpacity} from "react-native";
import styles from "./TypeOfOrderStyle";

const TypeOfOrder = props=>{
    return(
      <View style={styles.mainContainer}>
          <Text style={styles.message}>Which type of order you want to place ?</Text>

          <View style={{flexDirection:'row',marginTop:40}}>
              <TouchableOpacity style={styles.buttonTouchable}
              onPress={()=>props.onOrderTypePress(0)}><Text style={styles.buttonText}>Dine in</Text></TouchableOpacity>
              <TouchableOpacity style={styles.buttonTouchable}
              onPress={()=>props.onOrderTypePress(1)}><Text style={styles.buttonText}>Takeout</Text></TouchableOpacity>
          </View>

      </View>
    );
};

export default TypeOfOrder