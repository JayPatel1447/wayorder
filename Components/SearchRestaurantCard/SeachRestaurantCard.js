//Author : Rohan Patel
import React from "react";
import {View, Text, Image} from "react-native";
import styles from "./SearchRestaurantCardStyle";

const SearchRestaurantCard = props => {
    return (
        <View style={styles.mainContainer}
              onStartShouldSetResponder={props.openOrderTypeDialog}>
            {/*Image View*/}
            <View style={styles.imageContainer}>
                <Image source={{uri: `${props.restaurant.bgImg}`}}
                       style={styles.imageStyle}/>
            </View>

            {/*Data View*/}
            <View style={styles.dataView}>
                <View style={{flexDirection: 'row'}}>
                    <View style={styles.logoContainer}>
                        <Image resizeMode={'cover'} source={{uri: `${props.restaurant.logo}`}}
                               style={{height: '100%', width: '100%'}}/>
                    </View>
                    <View style={{flex: 4}}>
                        <Text style={styles.name}>{props.restaurant.name}</Text>
                        <Text style={styles.address}>5995 McLaughlin Rd</Text>
                        <Text style={styles.distance}>1.5 km</Text>
                    </View>
                    <View style={{flex: 1, alignItems: 'center'}}>
                        <View style={styles.ratingView}>
                            <Text style={{color: 'white', fontWeight: '700', fontSize: 15}}>9.9</Text>
                        </View>
                    </View>
                </View>
            </View>
        </View>
    )
};

export default SearchRestaurantCard;