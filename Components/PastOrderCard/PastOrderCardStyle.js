//Author : Jay Patel
import {StyleSheet} from "react-native";

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    padding: 15,
  },
  image: {
    width: 100,
    height: 70,
    borderRadius: 10,
    marginRight: 10,
  },
  title: {
    fontWeight: "600",
    fontSize:16,
    marginBottom:5
  },
  subTitle: {
    color: "#6e6969",
    fontSize: 15
  },
  time: {
    color: "#6e6969",
  },
  DetailsContainer: {
    marginLeft: 20,
    justifyContent: "center",
    flex: 1,
  },
  icon: {
    paddingTop: 20,
  },
});
export default styles;
