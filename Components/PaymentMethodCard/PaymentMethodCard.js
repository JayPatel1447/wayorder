//Author : Jay Patel
import React from "react";
import {Image, Text, View} from "react-native";
import Styles from "./PaymentMethodCardStyle";

import GestureRecognizer, {swipeDirections} from 'react-native-swipe-gestures';

const PaymentMethodCard = props => {
    return (
        <GestureRecognizer config={{
            velocityThreshold: 0.3,
            directionalOffsetThreshold: 80
        }}
                           onSwipeLeft={() => (props.action === "delete") ? props.onSwipe() : {}}
        >
            <View style={Styles.mainContainer}>
                <View style={Styles.innerContainer}>
                    <View style={Styles.imageContainer}>
                        <Image source={require("../../assets/visaBlue.png")}
                               style={Styles.image}/>
                    </View>

                    <View style={Styles.numberContainer}>
                        <Text style={Styles.number}>{props.number}</Text>
                    </View>

                    <View style={Styles.actionContainer}
                          onResponderRelease={() => props.action.trim() != "" ? props.onPress() : {}}
                          onStartShouldSetResponder={() => true}
                    >
                        {props.action === "select" &&
                        <View style={[Styles.radioButton, props.selected ? {} : {backgroundColor: 'white'}]}/>}
                    </View>
                </View>
            </View>
        </GestureRecognizer>
    );
};

export default PaymentMethodCard;