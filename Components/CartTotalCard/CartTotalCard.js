//Author : Rohan Patel
import React from "react";
import {Text, View} from "react-native";
import styles from "./CartTotalCardStyle";

const CartTotalCard = props => {
    return (
        <View style={styles.mainContainer}>
            <View style={{flexDirection: 'row'}}>
                <View style={{flex: 3}}>
                    <Text style={styles.normalFontBill}>Food And Beverages
                        Subtotal</Text>
                </View>
                <View style={{flex: 1, alignItems: 'flex-end'}}>
                    <Text
                        style={styles.normalFontBill}>${props.subTotal.toFixed(2)}</Text>
                </View>
            </View>
            <View style={{flexDirection: 'row', marginTop: 15}}>
                <View style={{flex: 3}}>
                    <Text style={styles.normalFontBill}>HST</Text>
                </View>
                <View style={{flex: 1, alignItems: 'flex-end'}}>
                    <Text
                        style={styles.normalFontBill}>${props.hst}</Text>
                </View>
            </View>
            <View style={{flexDirection: 'row', marginTop: 15,marginBottom:10}}>
                <View style={{flex: 3}}>
                    <Text style={{fontSize: 22, color: '#929395'}}>Total</Text>
                </View>
                <View style={{flex: 1, alignItems: 'flex-end'}}>
                    <Text
                        style={{fontSize: 22, fontWeight: 'bold'}}>${props.cartTotal}</Text>
                </View>
            </View>
        </View>
    );
};
export default CartTotalCard;