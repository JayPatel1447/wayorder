//Author : Rohan Patel
import React from "react";
import {createStackNavigator} from "@react-navigation/stack";
import MenuScreen from "../../Screens/MenuScreen/MenuScreen";
import HomeScreen from "../../Screens/HomeScreen/HomeScreen";
import CartScreen from "../../Screens/CartScreen/CartScreen";
import MenuItem from "../../Screens/MenuItemScreen/MenuItem";
import CheckoutScreen from "../../Screens/CheckoutScreen/CheckoutScreen";
import AddPaymentMethod from "../../Screens/AddPaymentMethodScreen/AddPaymentMethodScreen";
import TakeoutInfoScreen from "../../Screens/TakeoutInfoScreen/TakeoutInfoScreen";
import TableReservationScreen from "../../Screens/TableReservationScreen/TableReservationScreen";

const HomeScreenContainer = props => {
    const Home = createStackNavigator();

    return (
        <Home.Navigator
            screenOptions={{
                headerShown: false
            }}>
            <Home.Screen name="Home" component={HomeScreen}
                         options={{
                             headerShown: false
                         }}/>
            <Home.Screen name="MenuScreen" component={MenuScreen}
                         options={{
                             headerShown: false, headerBackTitle: 'Home',
                             headerTitle: '', headerTransparent: true,
                             headerTintColor: 'black'
                         }}/>
            <Home.Screen name="TakeoutInfo" component={TakeoutInfoScreen}
                         options={{
                             headerShown: true, headerBackTitle: 'Order',
                             headerTitle: '', headerTransparent: true,
                             headerTintColor: 'black'
                         }}/>
            <Home.Screen name="TableReservation" component={TableReservationScreen}
                         options={{
                             headerShown: true, headerBackTitle: 'Order',
                             headerTitle: 'Reserve Table', headerTransparent: true,
                             headerTintColor: 'black'
                         }}/>
            <Home.Screen
                options={{
                    headerShown: true, headerBackTitle: 'Menu', headerTintColor: 'white',
                    headerTransparent: true, headerTitle: ''
                }}
                name="CartScreen" component={CartScreen}/>
            <Home.Screen
                options={{
                    headerShown: true, headerBackTitle: 'Menu',
                    headerTitle: '', headerTransparent: true,
                    headerTintColor: 'white'
                }}
                name="MenuItem" component={MenuItem}/>

            <Home.Screen
                options={{
                    headerShown: true, headerBackTitle: 'Order',
                    headerTitle: '', headerTransparent: true,
                    headerTintColor: 'black'
                }}
                name="Checkout" component={CheckoutScreen}/>

            <Home.Screen name="AddNewPaymentMethod" component={AddPaymentMethod}
                         options={{
                             headerShown: true, headerBackTitle: 'Checkout',
                             headerTitle: '', headerTransparent: true,
                             headerTintColor: 'black'
                         }}/>

        </Home.Navigator>
    )
};

export default HomeScreenContainer