//Author : Rohan Patel
import React from 'react';
import {createStackNavigator} from "@react-navigation/stack";
import IncorrectScanScreen from "../../Screens/IncorrectScan/IncorrectScanScreen";
import ScanScreen from "../../Screens/ScanScreen/ScanScreen";
import MenuScreenContainer from "../../Screens/MenuScreen/MenuScreen";
import MenuItem from "../../Screens/MenuItemScreen/MenuItem";
import CartScreen from "../../Screens/CartScreen/CartScreen";
import CheckoutScreen from "../../Screens/CheckoutScreen/CheckoutScreen";
import AddPaymentMethod from "../../Screens/AddPaymentMethodScreen/AddPaymentMethodScreen";


const ScanScreenContainer = props => {

    const Scan = createStackNavigator();
    return (
        <Scan.Navigator
            screenOptions={{
                headerShown: false
            }}>
            <Scan.Screen name="Scan" component={ScanScreen}/>
            <Scan.Screen name="IncorrectScan" component={IncorrectScanScreen}/>
            <Scan.Screen name="MenuScreen" component={MenuScreenContainer}/>
            <Scan.Screen
                options={{
                    headerShown: true, headerBackTitle: 'Menu', headerTintColor: 'white',
                    headerTransparent: true, headerTitle: ''
                }}
                name="CartScreen" component={CartScreen}/>
            <Scan.Screen
                options={{
                    headerShown: true, headerBackTitle: 'Menu',
                    headerTitle: '', headerTransparent: true,
                    headerTintColor: 'white'
                }}
                name="MenuItem" component={MenuItem}/>

            <Scan.Screen
                options={{
                    headerShown: true, headerBackTitle: 'Order',
                    headerTitle: '', headerTransparent: true,
                    headerTintColor: 'black'
                }}
                name="Checkout" component={CheckoutScreen}/>

            <Scan.Screen name="AddNewPaymentMethod" component={AddPaymentMethod}
                         options={{
                             headerShown: true, headerBackTitle: 'Checkout',
                             headerTitle: '', headerTransparent: true,
                             headerTintColor: 'black'
                         }}/>

        </Scan.Navigator>
    );
}

export default ScanScreenContainer;