//Author : Rohan Patel
export const ADD_TO_CART = "ADD_TO_CART";
export const CLEAR_CART = "CLEAR_CART";
export const REMOVE_DISH = "REMOVE_DISH";