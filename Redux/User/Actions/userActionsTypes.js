//Author : Rohan Patel
export const UPDATE_FULLNAME = "UPDATE_FULLNAME";
export const UPDATE_EMAIL = "UPDATE_EMAIL";
export const UPDATE_PASSWORD = "UPDATE_PASSWORD";
export const UPDATE_USER = "UPDATE_USER";
export const ADD_ORDER = "ADD_ORDER";