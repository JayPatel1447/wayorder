//Author : Rohan Patel
import {UPDATE_RESTAURANT} from "../Actions/restaurantActionsTypes";

const initialState = {};

const restaurantReducer = (state = initialState, {type, payload}) => {
    switch (type) {
        case UPDATE_RESTAURANT:
            return payload;
        default :
            return state;
    }
};

export default restaurantReducer;