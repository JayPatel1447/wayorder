//Author : Rohan Patel
import {Dimensions, StyleSheet} from "react-native";

const dimension = Dimensions.get('window');

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        backgroundColor: "white",
        alignItems: "center"
    },
    nameContainer: {
        flex: 1,
        backgroundColor: "white",
        marginTop: '5%',
        marginLeft: '5%',
        alignSelf: "stretch"
    },
    contentContainer: {
        flex: 5,
        backgroundColor: 'white',
        alignSelf: "stretch",
        borderTopLeftRadius: 15,
        borderTopRightRadius: 15
    },
    name: {
        fontSize: 25,
    },
    button: {
        borderRadius: 10,
        height: 50,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row'
    },
    buttonContainer: {
        flex: 1,
        width: dimension.width,
        paddingLeft: '5%',
        paddingRight: '5%',
        paddingBottom: '10%',
        justifyContent: 'center'
    },
    buttonText: {
        color: 'white',
        fontSize: 16,
        fontWeight: "500"
    },
    gradientContainer: {
        height: "70%",
        width: "100%",
        alignItems: "center",
        justifyContent: "center",
        borderRadius: 20,
    }
});
export default styles;
