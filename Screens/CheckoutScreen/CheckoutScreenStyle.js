//Author : Rohan Patel
import {Dimensions, StyleSheet} from "react-native";

const dimension = Dimensions.get("window");
const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        backgroundColor: 'white'
    },
    v1: {
        flex: 5,
        marginTop: 70
    },
    v2: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    selectText: {
        fontSize: 23,
        fontWeight: '500',
        marginLeft: 25
    },
    addNewPaymentMethodTouchable: {
        alignItems: 'center',
        justifyContent: 'center'
    },
    addNewPaymentMethodText: {
        fontSize: 16,
        fontWeight: 'bold',
        color: '#373B95',
        marginBottom: '5%'
    }
});
export default styles;
