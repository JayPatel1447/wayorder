//Author : Rohan Patel
import React, {useState} from "react";
import {SafeAreaView, View, Text, Image, TouchableOpacity} from "react-native";
import styles from "./TakeoutInfoScreenStyle";
import {Entypo, FontAwesome5, AntDesign, FontAwesome} from '@expo/vector-icons';
import GradientButton from "../../Components/GradientButton/GradientButton";
import CartTotalCard from "../../Components/CartTotalCard/CartTotalCard";
import DateTimePicker from '@react-native-community/datetimepicker';
import DateTimePickerModal from "../../Components/DateTimePickerModal/DateTimePickerModal";
import RNDateTimePicker from "@react-native-community/datetimepicker";
import {min} from "react-native-reanimated";

const TakeoutInfoScreen = props => {
    const {subTotal} = props.route.params;
    const {hst} = props.route.params;
    const {cartTotal} = props.route.params;
    const {placeOrderHandler} = props.route.params

    const [showModal, setShowModal] = useState(false)
    const [takeoutTime, setTakeoutTime] = useState(null);
    const onChangePressHandler = date => {
        setTakeoutTime(date);
    };

    const onTakeOutPayHandler = ()=>{
        if(takeoutTime === null){
            const date = new Date();
            date.setTime(date.getTime()+ 15*60000)
            const hours = date.getHours().toString().length==1 ? "0"+date.getHours() : date.getHours();
            const minutes =  date.getMinutes().toString().length==1 ? "0"+date.getMinutes() : date.getMinutes();
            const seconds = date.getSeconds().toString().length==1 ? "0"+date.getSeconds() : date.getSeconds();

            const time = hours+":"+minutes+":"+seconds;
            placeOrderHandler({date:date,time:time})
        }else {
            takeoutTime.date = takeoutTime.date-1;
            const hours = takeoutTime.getHours().toString().length==1 ? "0"+takeoutTime.getHours() : takeoutTime.getHours();
            const minutes =  takeoutTime.getMinutes().toString().length==1 ? "0"+takeoutTime.getMinutes() : takeoutTime.getMinutes();
            const seconds = takeoutTime.getSeconds().toString().length==1 ? "0"+takeoutTime.getSeconds() : takeoutTime.getSeconds();

            const time = hours+":"+minutes+":"+seconds;
            console.log("time is " +time);
            placeOrderHandler({date:takeoutTime,time:time});
        }
    };
    const renderTakeoutTime = () => {
        let displayTime = takeoutTime.toLocaleString('default', {weekday: 'short'});
        displayTime += " " + takeoutTime.toLocaleString('default', {month: 'short'});
        displayTime += " " + takeoutTime.getDate()
        displayTime += ", " + takeoutTime.toLocaleString('en-US', {hour: 'numeric', minute: 'numeric', hour12: true})
        return displayTime
    };

    return (
        <View style={styles.mainContainer}>
            <View style={styles.mapView}>
                <Image source={require("../../assets/GoogleMapTA.jpg")}
                       style={{height: '100%', width: '100%'}} resizeMode={"cover"}/>
            </View>
            <View style={styles.contentView}>
                <Text style={styles.yourTakeout}>Your Takeout</Text>
                <View style={styles.addressContainer}>
                    <View style={{flex: 1}}>
                        <Entypo name="location-pin" size={30} color="black"/>
                    </View>
                    <View style={styles.addressTextContainer}>
                        <Text style={styles.addressText}>517 Navigator Dr, Mississauga, ON, L5W1P5</Text>
                    </View>
                </View>
                <View style={styles.directionContainer}>
                    <View style={{flex: 1}}>
                        <FontAwesome5 name="directions" size={24} color="black" style={{paddingLeft: 3}}/>
                    </View>
                    <View style={styles.addressTextContainer}>
                        <TouchableOpacity><Text style={styles.directionText}>GET DIRECTIONS</Text></TouchableOpacity>
                    </View>
                </View>

                <View style={styles.timeContainer}>
                    <View style={{flex: 1}}>
                        <AntDesign name="clockcircle" size={24} color="black" style={{paddingLeft: 3}}/>
                    </View>
                    <View style={styles.timeTextContainer}>
                        {takeoutTime === null && <Text style={styles.timeText}>ASAP (15 - 30 mins)</Text>}
                        {takeoutTime !== null && (<Text style={styles.timeText}>{renderTakeoutTime()}</Text>)}
                    </View>
                    <View style={styles.timeChangeContainer}>
                        <TouchableOpacity onPress={() => setShowModal(true)}>
                            <Text style={styles.timeChangeText}>CHANGE</Text>
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={styles.timeContainer}>
                    <View style={{flex: 1}}>
                        <FontAwesome name="credit-card-alt" size={22} color="black" style={{paddingLeft: 2}}/>
                    </View>
                    <View style={styles.timeTextContainer}>
                        <Text style={styles.timeText}>**** **** **** 1234</Text>
                    </View>
                    <View style={styles.timeChangeContainer}>
                        <Text style={styles.timeChangeText}>CHANGE</Text>
                    </View>
                </View>


                <View style={{flex: 3, alignItems: 'center', paddingTop: 50}}>
                    <CartTotalCard subTotal={subTotal} hst={hst} cartTotal={cartTotal}/></View>
                <SafeAreaView style={{flex: 2, alignItems: 'center', justifyContent: 'center'}}>
                    {/* <GradientButton text={"Pay Now"} onPressHandler={() => placeOrderHandler()}/>*/}
                    <GradientButton text={"Pay Now"} onPressHandler={() => onTakeOutPayHandler()}/>
                </SafeAreaView>

                <DateTimePickerModal modalVisible={showModal} onCancelPressHandler={() => setShowModal(false)}
                                     onChangePressHandler={(date) => onChangePressHandler(date)
                                     }/>
            </View>
        </View>
    );
};

export default TakeoutInfoScreen