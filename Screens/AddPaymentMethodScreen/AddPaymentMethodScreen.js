//Author : Rohan Patel
import React, {useState} from "react";
import {SafeAreaView, Text, TouchableOpacity, View} from "react-native";
import CreditCard from "../../Components/CreditCard/CreditCard";
import Styles from "./AddPaymentMethodScreenStyle";
import {KeyboardAwareScrollView} from "react-native-keyboard-aware-scroll-view";
import {OutlinedTextField} from "react-native-material-textfield";
import styles from "../LoginScreen/LoginScreenStyle";
import {LinearGradient} from "expo-linear-gradient";
import GradientButton from "../../Components/GradientButton/GradientButton";

const AddPaymentMethod = props => {
    const [cardNumber, setCardNumber] = useState("1234     5678     7845     1234");
    const [nameOnCard, setNameOnCard] = useState("Mr. Rohan Patel");
    const [expDate, setExpDate] = useState("22/22");
    const [cvv, setCvv] = useState(123);

    const onCardNumberChangeHandler = num => {
        setCardNumber(num);
    };

    const onNameChangeHandler = name => {
        setNameOnCard(name);
    };

    const onExpChangeHandler = exp => {
        setExpDate(exp);
    };

    const onCvvChangeHandler = c => {
        setCvv(c);
    };

    return (
        <KeyboardAwareScrollView
            style={{backgroundColor: "white"}}
            resetScrollToCoords={{x: 0, y: 0}}
            contentContainerStyle={Styles.mainContainer}
            scrollEnabled={false}
        >
            <SafeAreaView style={{flex: 1, marginTop: '25%'}}>
                <CreditCard card={{number: cardNumber, name: nameOnCard, exp: expDate}}/>
            </SafeAreaView>

            <SafeAreaView style={{flex: 2, alignItems: 'center'}}>

                <OutlinedTextField
                    label="Card Number"
                    containerStyle={{width: '85%', marginTop: '3%'}}
                    onChangeText={(text) => onCardNumberChangeHandler(text)}
                />
                <OutlinedTextField
                    label="Name on card"
                    containerStyle={{width: '85%', marginTop: '3%'}}
                    onChangeText={(text) => onNameChangeHandler(text)}
                />
                <View style={{flexDirection: 'row', width: '85%', marginTop: '3%'}}>
                    <View style={{flex: 1}}>
                        <OutlinedTextField
                            label="EXP"
                            containerStyle={{width: '55%'}}
                            onChangeText={(text) => onExpChangeHandler(text)}
                        />
                    </View>
                    <View style={{flex: 1, alignItems: 'flex-end'}}>
                        <OutlinedTextField
                            label="CVV"
                            containerStyle={{width: '55%'}}
                            onChangeText={(text) => onCvvChangeHandler(text)}
                        />
                    </View>
                </View>


                <View style={{
                    width: '100%',
                    flexDirection: 'row',
                    flex: 1,
                    justifyContent: 'center',
                    alignItems: 'flex-end',
                    paddingBottom: 25
                }}>

                    <GradientButton onPressHandler={() => {
                    }} text={"ADD CARD"}/>
                </View>

            </SafeAreaView>


        </KeyboardAwareScrollView>
    );
};

export default AddPaymentMethod;