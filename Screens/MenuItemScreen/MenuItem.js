//Author : Rohan Patel
import React, {useState} from "react";
import {
    Text,
    View,
    Image,
    TextInput,
    Keyboard,
} from "react-native";
import styles from "./MenuItemStyle";
import * as Haptics from "expo-haptics";
import {ImpactFeedbackStyle} from "expo-haptics";
import {connect} from "react-redux";
import {addToCart} from "../../Redux/Cart/Actions/cartActions";
import GradientButton from "../../Components/GradientButton/GradientButton";

const MenuItem = (props) => {
    const dish = props.route.params.dish;

    const [quantity, setQuantity] = useState(1);
    const [note, setNote] = useState("");

    const onPlusClickHandler = () => {
        setQuantity(quantity + 1);
    };

    const onMinusClickHandler = () => {
        if (quantity !== 1) setQuantity(quantity - 1);
        else {
            Haptics.impactAsync(ImpactFeedbackStyle.Heavy);
            console.log("already 1");
        }
    };

    const onAddToCartClickHandler = () => {
        console.log("ADD TO CART CLICKED");
        //console.log(note);
        console.log("AFTER NOTES LOG");

        const orderItem = {
            id: props.cart.length + 1,
            dish: dish,
            note: note,
            quantity: Number(quantity),
        };
        props.onAddToCart(orderItem);
        props.navigation.navigate("MenuScreen");
    };

    return (
        <View style={styles.mainContainer}>
            <View style={styles.menuImageView}>
                <Image
                    style={styles.menuImage}
                    source={{
                        uri: `${dish.image}`,
                    }}
                >

                </Image>
            </View>

            <View style={styles.menuItemContent}>
                <Text style={styles.dishName}>{dish.name}</Text>
                <Text style={styles.menuItemDesc}>{dish.description}</Text>

                <View style={{flexDirection: 'row', marginTop: '8%', marginLeft: '5%', marginRight: '5%'}}>
                    <View style={{flex: 1}}>
                        <Text style={{fontWeight: 'bold', fontSize: 22}}>Price</Text>
                        <View style={{flexDirection: 'row', marginTop: '5%'}}>
                            <Text style={styles.dollarSymbol}>$</Text>
                            <Text style={styles.price}>{dish.price}</Text>
                        </View>
                    </View>


                    <View style={{flex: 1, alignItems: 'flex-end'}}>
                        <Text style={{fontWeight: 'bold', fontSize: 22}}>Quantity</Text>

                        <View style={styles.quantityContainer}>
                            <View style={styles.minusContainer} onResponderRelease={() => onMinusClickHandler()}
                                  onStartShouldSetResponder={() => true}>
                                <Text style={{color: 'white', fontSize: 19}}>-</Text>
                            </View>
                            <View style={styles.number}>
                                <Text style={{fontWeight: 'bold', fontSize: 15}}>{quantity}</Text>
                            </View>
                            <View style={styles.plusContainer} onResponderRelease={() => onPlusClickHandler()}
                                  onStartShouldSetResponder={() => true}>
                                <Text style={{color: 'white', fontSize: 19}}>+</Text>
                            </View>
                        </View>
                    </View>
                </View>

                <View style={styles.noteContainer}>
                    <TextInput
                        style={styles.textArea}
                        maxLength={120}
                        placeholder={"Add notes (eg. extra sauce, onion, etc)"}
                        keyboardType="default"
                        returnKeyType="done"
                        multiline={true}
                        blurOnSubmit={true}
                        onSubmitEditing={() => {
                            Keyboard.dismiss();
                        }}
                        value={note}
                        onChangeText={(text) => setNote(text)}
                    />
                </View>
            </View>
            <View style={styles.buttonContainer}>
                <GradientButton text={"ADD TO ORDER"} onPressHandler={() => onAddToCartClickHandler()}/>
            </View>
        </View>
    );
};

const mapStateToProps = (state) => {
    return {
        cart: state.cart.items,
    };
};

const mapActionToProps = {
    onAddToCart: addToCart,
};
export default connect(mapStateToProps, mapActionToProps)(MenuItem);
