//Author : Rohan Patel
import {Dimensions, StyleSheet} from "react-native";

const dimension = Dimensions.get("window");
const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        backgroundColor: "white",
        alignItems: "center",
    },
    menuImageView: {
        flex: 2,
        width: '100%',
        borderBottomLeftRadius: 30,
        borderBottomRightRadius: 30
    },
    menuImage: {
        width: dimension.width,
        height: Math.round((dimension.width * 11) / 16),
        resizeMode: "cover",
        display: "flex",
        flexDirection: "row",
        borderBottomLeftRadius: 30,
        borderBottomRightRadius: 30
    },
    menuItemContent: {
        flex: 3,
        width: dimension.width,
        marginTop: "10%",
    },
    menuItemName: {
        fontSize: 30,
        color: "white",
        alignSelf: "flex-end",
        marginRight: "5%",
        marginLeft: "5%",
        marginBottom: "2%",
    },
    menuItemDesc: {
        fontSize: 14,
        color: "grey",
        marginLeft: "4%",
        fontWeight: '600'
    },
    minus: {
        fontSize: 30,
        color: "white",
    },
    minusContainer: {
        justifyContent: "center",
        alignItems: 'center',
        backgroundColor: "black",
        height: 35,
        width: 35,
        borderRadius: 50
    },
    plus: {
        fontSize: 30,
        color: "white",
    },
    plusContainer: {
        alignItems: 'center',
        justifyContent: "center",
        backgroundColor: "#FA7B4F",
        height: 35,
        width: 35,
        borderRadius: 50
    },
    number: {
        width: 45,
        justifyContent: 'center',
        alignItems: 'center',
        height: 35
    },
    numberContainer: {
        paddingLeft: 20,
        paddingRight: 20,
        justifyContent: "center",
    },
    dishName: {
        fontSize: 33,
        marginLeft: '4%',
        marginBottom: '3%',
        fontWeight: '600'
    },
    quantityContainer: {
        flexDirection: 'row', borderRadius: 50, backgroundColor: 'white', marginTop: '6%',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 5,
        },
        shadowOpacity: 0.25,
        shadowRadius: 6.27,

        elevation: 10,
    },
    noteContainer: {
        width: dimension.width,
        marginTop: 20,
    },
    textArea: {
        marginLeft: 20,
        marginRight: 20,
        borderRadius: 15,
        textAlignVertical: "top", // hack android
        height: 120,
        fontSize: 14,
        color: "#333",
        paddingTop: '3%',
        paddingLeft: '4%',
        backgroundColor: 'white',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 5,
        },
        shadowOpacity: 0.25,
        shadowRadius: 6.27,

        elevation: 10,

    },
    button: {
        backgroundColor: "black",
        borderRadius: 10,
        height: 50,
        justifyContent: "center",
        alignItems: "center",
    },
    buttonContainer: {
        flex: 0.7,
        width: dimension.width,
        height: 100,
        paddingLeft: "10%",
        paddingRight: "10%",
        justifyContent: "center",
        alignItems: 'center',

    },
    buttonText: {
        color: "white",
        fontSize: 18,
        fontWeight: "500",
    },
    addToOrderContainer: {
        height: 58,
        alignItems: "center",
        justifyContent: "center",
        width: "80%",
        shadowColor: "#000",
        shadowOffset: {width: 0, height: 12},
        shadowOpacity: 0.1,
        shadowRadius: 48,
        elevation: 3
    },
    gradientContainer: {
        height: "100%",
        width: "100%",
        alignItems: "center",
        justifyContent: "center",
        borderRadius: 30,
    },
    addToOrderTouchable: {
        height: "100%",
        width: "100%",
        alignItems: "center",
        justifyContent: "center",
    },
    addToOrderText: {
        color: "white",
        fontSize: 19,
        fontWeight: "bold",
    },
    dollarSymbol: {
        fontSize: 18, paddingTop: '4%', fontWeight: 'bold', color: '#FA7B4F'
    },
    price: {
        fontSize: 28, fontWeight: 'bold', marginLeft: '3%'
    }
});
export default styles;
