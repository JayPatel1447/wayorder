//Author : Rohan Patel
import React from "react";
import {SafeAreaView, Text, TouchableOpacity, View} from "react-native";
import styles from "./IncorrectScanStyle";
import LottieView from "lottie-react-native/src/js";
import {connect} from "react-redux";
import {updateRestaurant} from "../../Redux/Restaurant/Actions/restaurantActions";

const IncorrectScanScreen = (props) => {
    return (
        <SafeAreaView style={styles.mainContainer}>
            <View style={{flex: 1}}>
                <LottieView
                    source={require("../../assets/handshake.json")}
                    autoPlay
                    loop={true}
                    style={styles.lottieImage}
                />
            </View>
            <View style={{flex: 1, alignItems: "center", paddingTop: "30%"}}>
                <Text style={{color: "white", fontSize: 45, fontWeight: "bold"}}>
                    Oops!
                </Text>
                <Text
                    style={{
                        color: "white",
                        fontSize: 15,
                        fontWeight: "400",
                        marginTop: "3%",
                    }}
                >
                    We cannot find the restaurant
                </Text>
                <TouchableOpacity
                    style={styles.scanAgainButton}
                    onPress={() => {
                        props.onRemoveRestaurant({});
                        props.navigation.navigate("Scan");
                    }}
                >
                    <Text style={{color: "white", fontSize: 18}}>Scan again</Text>
                </TouchableOpacity>
            </View>
        </SafeAreaView>
    );
};

const mapStateToProps = (state) => ({
    restaurant: state.restaurant,
});
const mapActionsToProps = {
    onRemoveRestaurant: updateRestaurant,
};

export default connect(mapStateToProps, mapActionsToProps)(IncorrectScanScreen);
